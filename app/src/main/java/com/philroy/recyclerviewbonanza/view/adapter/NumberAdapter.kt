package com.philroy.recyclerviewbonanza.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.philroy.recyclerviewbonanza.databinding.ListItemBinding

class NumberAdapter : RecyclerView.Adapter<NumberAdapter.NumberViewHolder>() {

    private lateinit var data: List<Int>

    class NumberViewHolder(
        private val binding: ListItemBinding
        ): RecyclerView.ViewHolder(binding.root) {
        fun bind(number: Int) {
            binding.tvItem.text = number.toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NumberViewHolder {
        val binding = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NumberViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun giveNumbers(numbers: List<Int>){
        data = numbers
    }
}