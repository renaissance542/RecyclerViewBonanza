package com.philroy.recyclerviewbonanza.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.philroy.recyclerviewbonanza.databinding.ListItemBinding

class WordsAdapter : RecyclerView.Adapter<WordsAdapter.WordViewHolder>() {

    private lateinit var words: List<String>

    class WordViewHolder(
        private val binding: ListItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(color: String){
            binding.tvItem.text = color
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val binding = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WordViewHolder(binding)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        holder.bind(words[position])
    }

    override fun getItemCount(): Int {
        return words.size
    }

    fun giveWords(words: List<String>){
        this.words = words
    }
}