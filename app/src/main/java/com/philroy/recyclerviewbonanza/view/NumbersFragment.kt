package com.philroy.recyclerviewbonanza.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.philroy.recyclerviewbonanza.databinding.FragmentNumbersBinding
import com.philroy.recyclerviewbonanza.view.adapter.NumberAdapter
import com.philroy.recyclerviewbonanza.viewmodel.NumbersViewModel

class NumbersFragment : Fragment() {
    private var _binding: FragmentNumbersBinding? = null
    private val binding get() = _binding!!

    private val mainViewModel = NumbersViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentNumbersBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainViewModel.numbers.observe(viewLifecycleOwner) {
            binding.rvNumbers.layoutManager = LinearLayoutManager(context) // why not requireContext()
            binding.rvNumbers.adapter = NumberAdapter().apply { giveNumbers(it) }
        }
        binding.btnFetchNumbers.setOnClickListener {
            mainViewModel.getNumbers()
        }
    }
}