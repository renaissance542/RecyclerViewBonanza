package com.philroy.recyclerviewbonanza.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.philroy.recyclerviewbonanza.databinding.FragmentTodoBinding
import com.philroy.recyclerviewbonanza.view.adapter.TodoAdapter
import com.philroy.recyclerviewbonanza.viewmodel.TodoViewModel

class TodoFragment : Fragment() {
    private var _binding: FragmentTodoBinding? = null
    private val binding get() = _binding!!

    private val viewModel = TodoViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentTodoBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        initObserver()
    }

    private fun initObserver() {
        viewModel.todos.observe(viewLifecycleOwner) {
            binding.rvTodo.layoutManager = LinearLayoutManager(context)
            binding.rvTodo.adapter = TodoAdapter().apply { giveTodos(it) }
        }
    }

    private fun initListeners() {
        binding.btnAddTodo.setOnClickListener {
            val item = binding.editTodo.text.toString()
            if (item != null) {
                viewModel.addTodo(item)
            }
        }
    }
}