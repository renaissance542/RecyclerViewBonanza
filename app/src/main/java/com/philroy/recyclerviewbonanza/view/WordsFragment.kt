package com.philroy.recyclerviewbonanza.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.philroy.recyclerviewbonanza.databinding.FragmentWordsBinding
import com.philroy.recyclerviewbonanza.view.adapter.WordsAdapter
import com.philroy.recyclerviewbonanza.viewmodel.WordsViewModel

class WordsFragment : Fragment() {
    private var _binding: FragmentWordsBinding? = null
    private val binding get() = _binding!!

    private val mainViewModel = WordsViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentWordsBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnFetchWords.setOnClickListener {
            mainViewModel.getWords()
        }
        initObservers()
    }

    fun initObservers() {
        mainViewModel.words.observe(viewLifecycleOwner) {
            binding.rvWords.layoutManager = LinearLayoutManager(context)
            binding.rvWords.adapter = WordsAdapter().apply { giveWords(it) }
        }
    }
}