package com.philroy.recyclerviewbonanza.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.philroy.recyclerviewbonanza.databinding.FragmentFirstBinding

class FirstFragment : Fragment () {
        private var _binding: FragmentFirstBinding? = null
        private val binding get() = _binding!!

        override fun onCreateView(
                inflater: LayoutInflater,
                container: ViewGroup?,
                savedInstanceState: Bundle?
        ) = FragmentFirstBinding.inflate(inflater, container, false).also {
                _binding = it
        }.root

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
                super.onViewCreated(view, savedInstanceState)
                initListeners()
        }

        private fun initListeners() {
                binding.btnNumbers.setOnClickListener {
                        val action = FirstFragmentDirections.actionFirstFragmentToNumbersFragment()
                        findNavController().navigate(action)
                }
                binding.btnTodo.setOnClickListener {
                        val action = FirstFragmentDirections.actionFirstFragmentToTodoFragment()
                        findNavController().navigate(action)
                }
                binding.btnWords.setOnClickListener {
                        val action = FirstFragmentDirections.actionFirstFragmentToWordsFragment()
                        findNavController().navigate(action)
                }

        }
}