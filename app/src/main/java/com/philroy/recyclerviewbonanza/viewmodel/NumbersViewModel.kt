package com.philroy.recyclerviewbonanza.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.philroy.recyclerviewbonanza.model.Repo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NumbersViewModel : ViewModel()   {

    private val repo = Repo

    private var _numbers = MutableLiveData<List<Int>>()
    val numbers: LiveData<List<Int>> get() = _numbers

    fun getNumbers() {
        viewModelScope.launch(Dispatchers.Main){
            _numbers.value = repo.getNumbers()
        }
    }

}