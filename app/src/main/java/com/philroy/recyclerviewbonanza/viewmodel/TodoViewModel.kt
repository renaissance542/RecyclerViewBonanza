package com.philroy.recyclerviewbonanza.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.philroy.recyclerviewbonanza.model.Repo

class TodoViewModel : ViewModel()   {

    private val repo = Repo

    private var _todos = MutableLiveData<MutableList<String>>()
    val todos: LiveData<MutableList<String>> = _todos

    fun addTodo(todo: String) {
        if (todos.value != null) {
            _todos.value?.add(todo)
        } else {
            _todos.value = mutableListOf<String>(todo)
        }
    }

}