package com.philroy.recyclerviewbonanza.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.philroy.recyclerviewbonanza.model.Repo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WordsViewModel : ViewModel()   {

    private val repo = Repo

    private var _words = MutableLiveData<List<String>>()
    val words: LiveData<List<String>> = _words

    fun getWords() {
        viewModelScope.launch(Dispatchers.Main) {
            _words.value = repo.getWords()
        }
    }

}