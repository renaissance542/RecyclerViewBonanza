package com.philroy.recyclerviewbonanza.model

import com.philroy.recyclerviewbonanza.model.remote.NumbersApi
import com.philroy.recyclerviewbonanza.model.remote.WordsApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.random.Random

object Repo {
    private val wordsApi = object : WordsApi {
        override fun getWords(): List<String> {
            return listOf(
                "apple", "bear", "crater", "dispute", "esoteric", "frustrating", "gullible",
                "happy", "insipid", "jocular", "kindness", "leaning", "mundane", "nostril",
                "orca", "perfunctory", "qintar", "resolute", "summary", "titillate", "urinary",
                "villian", "watermelon", "xenon", "yessir", "ze_last_word"
            )
        }
    }

    private val numApi = object : NumbersApi {
        override fun getNumbers(): List<Int> {
            val result = mutableListOf<Int>()
            repeat(25) { result.add(Random.nextInt()) }
            return result
        }
    }

    suspend fun getWords(): List<String> = withContext(Dispatchers.IO) {
        return@withContext wordsApi.getWords()
    }

    suspend fun getNumbers(): List<Int> = withContext(Dispatchers.IO){
        return@withContext numApi.getNumbers()
    }
}