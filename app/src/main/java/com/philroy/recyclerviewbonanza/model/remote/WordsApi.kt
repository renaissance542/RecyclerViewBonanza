package com.philroy.recyclerviewbonanza.model.remote

interface WordsApi {
    fun getWords(): List<String>
}