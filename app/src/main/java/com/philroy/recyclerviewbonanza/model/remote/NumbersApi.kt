package com.philroy.recyclerviewbonanza.model.remote

interface NumbersApi {
    fun getNumbers(): List<Int>
}